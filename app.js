const cardList = document.querySelector(".cardList");
document.addEventListener('DOMContentLoaded', () => {
    getUsers()
        .then(usersData => getPosts(usersData));
});
const openModalBtn = document.querySelector('.createCardBtn');
openModalBtn.addEventListener('click', () => {
    const modalInstance = new Modal();
});

function getUsers() {
    return fetch('https://ajax.test-danit.com/api/json/users')
        .then(response => response.json())
        .catch(error => {
            console.error('Помилка отримання користувачів:', error);
            throw error;
        });
}
function getPosts(usersData) {
    fetch('https://ajax.test-danit.com/api/json/posts')
        .then(response => response.json())
        .then(postsData => {
            displayPosts(postsData, usersData);
        })
        .catch(error => console.error('Помилка отримання публікацій:', error));
}
function displayPosts(postsData, usersData) {
    postsData.forEach(post => {
        const userHisPosted = post.userId;
        const user = findObjectById(usersData, userHisPosted);
        const postElement = new Card(user, post);
        postElement.appendElementTo(cardList);
    });

    cardList.addEventListener('click', (event) => {
        if (event.target.classList.contains('card_deleteBtn')) {
            const postId = event.target.parentElement.parentElement.getAttribute('post-id');
            deletePost(postId, event.target.parentElement.parentElement);
        }
    });
}

function findObjectById(array, id) {
    return array.find(obj => obj.id === id);
}
function deletePost(postId, postElement) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (response.ok) {
                postElement.remove();
                console.log(`Пост з ID ${postId} видалено успішно.`);
            } else {
                console.error(`Помилка видалення посту з ID ${postId}.`);
            }
        })
        .catch(error => console.error('Помилка видалення посту:', error));
}

class Card {
    constructor(user, post) {
        this.user = user;
        this.post = post;
        this.element = this.createCardElement();
    }
    createCardElement() {
        const card = document.createElement("div");
        card.classList.add("card");
        card.setAttribute("post-id", `${this.post.id}`)

        const cardHeader = document.createElement("div");
        cardHeader.classList.add("card_header");

        const userInfoContainer = document.createElement("div");
        userInfoContainer.classList.add("card_userInfo");

        const avatar = document.createElement("div");
        avatar.classList.add("card_userInfo_avatar");
        avatar.textContent = this.user.name.charAt(0);

        const userName = document.createElement("h2");
        userName.classList.add("card_userInfo_name");
        userName.textContent = `${this.user.name}`;

        const emailLink = document.createElement("a");
        emailLink.href = `mailto:${this.user.email}`;
        emailLink.textContent = `${this.user.email}`;

        const deleteBtn = document.createElement("button");
        deleteBtn.classList.add("card_deleteBtn");

        userName.append(emailLink);
        userInfoContainer.append(avatar);
        userInfoContainer.append(userName);
        cardHeader.append(userInfoContainer);
        cardHeader.append(deleteBtn);

        const postContainer = document.createElement("div");
        postContainer.classList.add("card_post");

        const postTitle = document.createElement("h2");
        postTitle.classList.add("card_post_title");
        postTitle.textContent = this.post.title;

        const postTxt = document.createElement("p");
        postTxt.classList.add("card_post_txt");
        postTxt.textContent = this.post.body;

        postContainer.append(postTitle);
        postContainer.append(postTxt);

        card.append(cardHeader);
        card.append(postContainer);

        return card;
    }
    appendElementTo(parent) {
        parent.prepend(this.element);
    }
}

class Modal {
    constructor() {
        this.modalElement = document.createElement('div');
        this.modalElement.id = 'myModal';
        document.body.appendChild(this.modalElement);

        this.titleInput = document.createElement('input');
        this.titleInput.type = 'text';
        this.titleInput.id = 'modal_title';
        this.titleInput.required = true;

        this.contentInput = document.createElement('textarea');
        this.contentInput.id = 'modal_content';
        this.contentInput.required = true;

        this.createBtn = document.createElement('button');
        this.createBtn.textContent = 'Створити';
        this.createBtn.addEventListener('click', () => this.createPublication());

        this.closeBtn = document.createElement('button');
        this.closeBtn.textContent = 'Закрити';
        this.closeBtn.addEventListener('click', () => this.closeModal());

        this.modalTitle = document.createElement('h2');
        this.modalTitle.textContent = 'Створення публікації'

        this.titleLabel = document.createElement('label');
        this.titleLabel.textContent = 'Заголовок:'

        this.br = document.createElement('br');
        this.br2 = document.createElement('br');

        this.contentLable = document.createElement('label');
        this.contentLable.textContent = 'Текст публікації:';

        this.modalElement.append(this.modalTitle);
        this.modalElement.append(this.titleLabel);
        this.modalElement.append(this.titleInput);
        this.modalElement.append(this.br);
        this.modalElement.append(this.contentLable);
        this.modalElement.append(this.contentInput);
        this.modalElement.append(this.br2);
        this.modalElement.append(this.createBtn);
        this.modalElement.append(this.closeBtn);
    }
    closeModal() {
        this.modalElement.remove();
    }
    sendPostRequest(post) {
        return fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(post)
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error(`error!Status: ${response.status}`);
                }
                return response.json();
            })
            .catch(error => {
                console.error('Error during POST request:', error);
            });
    }
    createPublication() {
        const title = this.titleInput.value;
        const content = this.contentInput.value;

        const post = {
            userId: 1,
            title: title,
            body: content,
        }
        this.sendPostRequest(post)
            .then(() => getUsers())
            .then(usersData => {
                getPosts(usersData);
            })
            .catch(error => console.error('Помилка при створенні публікації:', error))
            .finally(() => this.closeModal());
    }
}